package net.shotbow.puzzle.commands;

import net.shotbow.puzzle.Puzzle;
import net.shotbow.puzzle.PuzzleCommandLeaf;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;

public class Stop extends PuzzleCommandLeaf {
    @Override
    protected void execute(CommandSender sender, Puzzle puzzle, String[] args) {
        if(puzzle != null) {

        }
        Puzzle.stop(block.getWorld());
    }

    @Override
    public boolean isPlayerOnly() {
        return false;
    }

    @Override
    public boolean isBlockOnly() {
        return false;
    }

    @Override
    public String[] getHelpMessage() {
        return new String[]{"Stops a puzzle test. One puzzle per world."};
    }

    @Override
    public String[] getAliases() {
        return new String[]{"stop"};
    }
}
