package net.shotbow.puzzle.commands;

import net.shotbow.puzzle.Puzzle;
import net.shotbow.puzzle.PuzzleCommandLeaf;
import org.bukkit.command.CommandSender;

public class Complete extends PuzzleCommandLeaf {

    @Override
    protected void execute(CommandSender sender, Puzzle puzzle, String[] args) {
        puzzle.onComplete();
    }

    @Override
    public String[] getHelpMessage() {
        return new String[]{"Completes the current puzzle"};
    }

    @Override
    public String[] getAliases() {
        return new String[]{"complete"};
    }

}
