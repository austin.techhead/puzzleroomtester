package net.shotbow.puzzle.commands;

import net.shotbow.puzzle.Puzzle;
import net.shotbow.puzzle.PuzzleCommandLeaf;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;

public class SpawnMob extends PuzzleCommandLeaf {

    @Override
    protected void execute(CommandSender sender, Puzzle puzzle, String[] args) {
        Location location = block.getRelative(BlockFace.UP, 3).getLocation().add(.5, 0, .5);
        EntityType entityType = EntityType.ZOMBIE;
        if(args.length > 2) {
            try {
                String xString = getCoordString(args[0]);
                String yString = getCoordString(args[1]);
                String zString = getCoordString(args[2]);
                double x = Double.parseDouble(xString);
                double y = Double.parseDouble(yString);
                double z = Double.parseDouble(zString);
                location = new Location(location.getWorld(), block.getX() + x, block.getY() + y, block.getZ() + z);
            } catch (Exception x) {
                sender.sendMessage(ChatColor.RED + "Unable to get relative coords from " + args[0] + " " + args[1] + " " + args[2] + " make sure they are ~# formatted (decimals acceptable)");
            }
        }
        if(args.length > 3) {
            try {
                entityType = EntityType.valueOf(args[3]);
            } catch (Exception x) {
                sender.sendMessage(ChatColor.RED + args[3] + " is nto an entity type. Check https://neolumia.github.io/spigot-docs/1.12.2/org/bukkit/entity/EntityType.html");
            }
        }
        location.getWorld().spawnEntity(location, entityType);
    }

    @Override
    public boolean isBlockOnly() {
        return false;
    }

    private String getCoordString(String arg) {
        if(arg.equals("~")) return "0";
        return arg.replace("~", "");
    }

    @Override
    public String[] getHelpMessage() {
        return new String[]{
                "Spawns a mob at the relative coordinate"
        };
    }

    @Override
    public String[] getAliases() {
        return new String[]{"spawnmob"};
    }
}
