package net.shotbow.puzzle.commands;

import net.shotbow.puzzle.Puzzle;
import net.shotbow.puzzle.PuzzleCommandLeaf;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;

public class Progress extends PuzzleCommandLeaf implements Listener {

    @Override
    protected void execute(CommandSender sender, Puzzle puzzle, String[] args) {
        // Handled in listener to capture both on && off states.
    }

    @Override
    public String[] getHelpMessage() {
        return new String[]{
                "Progresses the boss bar of the current puzzle,",
                "will revert progress if unpowered."
        };
    }

    @Override
    public String[] getAliases() {
        return new String[]{"progress"};
    }


    @EventHandler
    public void onRedstone(BlockRedstoneEvent e) {
        Puzzle puzzle = Puzzle.get(e.getBlock().getWorld());
        if(puzzle == null) return;
        if(isPuzzleComponent(e.getBlock())) {
            if(e.getNewCurrent() > 0 && e.getOldCurrent() == 0) {
                puzzle.progress();
            } else if(e.getOldCurrent() > 0 && e.getNewCurrent() == 0) {
                puzzle.unProgress();
            }
        }
    }

    private boolean isPuzzleComponent(Block block) {
        if(block.getType() != Material.COMMAND) return false;
        return isProgressCommandBlock(block);
    }

    private boolean isProgressCommandBlock(Block block) {
        CommandBlock commandBlock = (CommandBlock) block.getState();
        String command = commandBlock.getCommand();
        return command.startsWith("puzzle progress");
    }
}
