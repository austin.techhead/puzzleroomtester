package net.shotbow.puzzle.commands;

import net.shotbow.puzzle.Puzzle;
import net.shotbow.puzzle.PuzzleCommandLeaf;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Start extends PuzzleCommandLeaf {
    @Override
    protected void execute(CommandSender sender, Puzzle puzzle, String[] args) {
        if(args.length == 0) {
            sender.sendMessage(ChatColor.RED + "start {# of segments}");
            return;
        }
        if(puzzle != null) {

        }
        Puzzle.start(block.getWorld(), Integer.parseInt(args[0]));
    }

    @Override
    public boolean isPlayerOnly() {
        return false;
    }

    @Override
    public boolean isBlockOnly() {
        return false;
    }

    @Override
    public String[] getHelpMessage() {
        return new String[]{"start {# segments} - Starts a puzzle test. One puzzle per world."};
    }

    @Override
    public String[] getAliases() {
        return new String[]{"start"};
    }
}
