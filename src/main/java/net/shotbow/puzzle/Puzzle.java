package net.shotbow.puzzle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.metadata.FixedMetadataValue;

public class Puzzle {

    World world;
    private int segments;
    private int progress;
    BossBar bossBar;
    boolean complete;

    public static void start(World world, int segments) {
        Puzzle puzzle = new Puzzle(world, segments);
        world.setMetadata("sbpuzzle",new FixedMetadataValue(PuzzleRoomTester.i, puzzle));
    }

    public static void stop(World world) {
        Puzzle puzzle = get(world);
        if(puzzle != null) {
            puzzle.stop();
        }
    }

    private void stop() {
        bossBar.setVisible(false);
        bossBar.removeAll();
        world.removeMetadata("sbpuzzle", PuzzleRoomTester.i);
    }

    public static Puzzle get(World world) {
        return world.hasMetadata("sbpuzzle") ? (Puzzle) world.getMetadata("sbpuzzle").get(0).value() : null;
    }

    public Puzzle(World world, int segments) {
        this.world = world;
        this.segments = segments;
        bossBar = Bukkit.createBossBar(ChatColor.GOLD + "Puzzle Test", BarColor.GREEN, BarStyle.SOLID);
        world.getPlayers().forEach(it -> bossBar.addPlayer(it));
        bossBar.setProgress(0);
        bossBar.setVisible(true);
    }

    public boolean inProgress() {
        return !complete;
    }

    public void progress() {
        progress++;
        showProgress();
    }

    public void unProgress() {
        progress--;
        showProgress();
    }

    private void showProgress() {
        bossBar.setProgress(complete ? 1 : Math.min(progress / (double)segments, 1));
    }

    public void onComplete() {
        complete = true;
        bossBar.setProgress(1);
        bossBar.setTitle(ChatColor.GREEN + "COMPLETE");
        world.getPlayers().forEach(it -> it.playSound(it.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1));
    }
}
