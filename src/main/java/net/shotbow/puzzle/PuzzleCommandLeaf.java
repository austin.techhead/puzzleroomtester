package net.shotbow.puzzle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class PuzzleCommandLeaf implements CommandLeaf {

    protected Block block;

    @Override
    public boolean execute(CommandSender sender, Puzzle puzzle, String[] args, String prefix) {
        if(sender instanceof BlockCommandSender) {
            block = ((BlockCommandSender) sender).getBlock();
            if(requiresPuzzle() && puzzle == null) {
                return false;
            }
        } else if(isBlockOnly()) {
            Bukkit.getLogger().info("Received puzzle command from " + sender.getClass().getSimpleName() + " " + sender.getName());
            sender.sendMessage(ChatColor.RED + getAliases()[0] + " is for command blocks only. /puzzle should give you descriptions of how to use them.");
            return true;
        }
        if(sender instanceof Player) {
            block = ((Player) sender).getLocation().getBlock();
        }
        execute(sender, puzzle, args);
        return true;
    }

    protected abstract void execute(CommandSender sender, Puzzle puzzle, String[] args);

    protected boolean requiresPuzzle(){return true;}

    @Override
    public String getPermission() {
        return "puzzle.command";
    }

    @Override
    public boolean isPlayerOnly() {
        return false;
    }

    @Override
    public boolean isBlockOnly() {
        return true;
    }

    public boolean isDevCommand() {
        return false;
    }

}
