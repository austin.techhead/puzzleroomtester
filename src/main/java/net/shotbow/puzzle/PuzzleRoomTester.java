package net.shotbow.puzzle;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class PuzzleRoomTester extends JavaPlugin {
    public static PuzzleRoomTester i;

    public static void register(Listener it) {
        Bukkit.getPluginManager().registerEvents(it, i);
    }

    public void onEnable() {
        i = this;
        Bukkit.getPluginCommand("puzzle").setExecutor(new PuzzleCommand());
    }
}
