package net.shotbow.puzzle;

import org.bukkit.command.CommandSender;

public interface CommandLeaf {
    boolean execute(CommandSender sender, Puzzle puzzle, String[] args, String prefix);

    String[] getHelpMessage();

    String[] getAliases();

    String getPermission();

    boolean isPlayerOnly();

    boolean isBlockOnly();

    boolean isDevCommand();
}
