package net.shotbow.puzzle;


import net.shotbow.puzzle.commands.*;
import org.bukkit.ChatColor;

import java.util.Arrays;

public class PuzzleCommand extends CommandRoot {

    public PuzzleCommand() {
        super(
                "puzzle",
                ChatColor.GOLD + "[PUZZLE] " + ChatColor.RESET,
                Arrays.asList(
                        new Start(),
                        new Stop(),
                        new Progress(),
                        new Complete(),
                        new SpawnMob()
                )

        );
    }
}
