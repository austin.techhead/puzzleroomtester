package net.shotbow.puzzle;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * For the creation of large command systems.
 */
public abstract class CommandRoot implements CommandExecutor
{
    private final String baseCommand;
    private final String prefix;
    private final List<? extends CommandLeaf> commandLeaves;

    public CommandRoot(String baseCommand, String prefix, List<? extends CommandLeaf> commandLeaves)
    {
        this.baseCommand = baseCommand;
        this.prefix = prefix;
        this.commandLeaves = commandLeaves;
        commandLeaves.stream()
                .filter(it -> it instanceof Listener)
                .map(it -> (Listener)it)
                .forEach(PuzzleRoomTester::register);
    }

    public boolean onCommand(final CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!cmd.getName().equalsIgnoreCase(baseCommand) && !cmd.getAliases().contains(baseCommand))
        {
            Bukkit.getLogger().info("Tried to execute " + cmd.getName() + " with root class " + getClass().getSimpleName() +", which do not match! Aborting.");
            return true;
        }

        List<CommandLeaf> accessibleCommands = commandLeaves.stream().filter(c -> c.getPermission() == null || sender.hasPermission(c.getPermission())).collect(Collectors.toList());

        if(accessibleCommands.isEmpty())
        {
            Bukkit.getLogger().info(sender.getName() + " tried to run " + baseCommand + " but has no permissions");
            sender.sendMessage(ChatColor.RESET + "Unknown Command. Type \"/help\" for help.");
            return true;
        }

        if(args.length == 0)
        {
            showHelp(sender, accessibleCommands);
            return true;
        }

        HashMap<String, CommandLeaf> commandsMap = new HashMap<>();

        accessibleCommands.forEach(c -> Arrays.stream(c.getAliases()).forEach(a -> commandsMap.put(a, c)));

        if (args[0].equalsIgnoreCase("aliases") || args[0].equalsIgnoreCase("ali"))
        {
            if (args.length == 1)
            {
                sender.sendMessage(prefix + ChatColor.RED + "Please enter a command for aliases.");
                return true;
            }

            if (!commandsMap.containsKey(args[1].toLowerCase()))
            {
                sender.sendMessage(prefix + ChatColor.RED + "Invalid command entered.");
                return true;
            }

            String[] commandAliases = commandsMap.get(args[1].toLowerCase()).getAliases();

            if (commandAliases == null || commandAliases.length == 0)
            {
                sender.sendMessage(prefix + ChatColor.RED + "There are no aliases for this command");
                return true;
            }

            sender.sendMessage(prefix + ChatColor.GRAY + "---------------------------------");
            sender.sendMessage(prefix + ChatColor.GOLD + "Aliases for " + commandAliases[0] + " command");
            sender.sendMessage(prefix + ChatColor.GRAY + "---------------------------------");

            Arrays.stream(commandAliases).forEach(a -> sender.sendMessage(prefix + ChatColor.AQUA + a));

            return true;
        }

        if (!commandsMap.containsKey(args[0].toLowerCase()))
        {
            sender.sendMessage(prefix + ChatColor.RED + "Invalid command.");
            showHelp(sender, accessibleCommands);
            return true;
        }

        CommandLeaf command = commandsMap.get(args[0].toLowerCase());

        if (!(sender instanceof Player) && command.isPlayerOnly())
        {
            sender.sendMessage(prefix + ChatColor.RED + "Only a player can execute this command.");
            return true;
        }

        String[] newArgs = new String[args.length - 1];

        System.arraycopy(args, 1, newArgs, 0, args.length - 1);

        Puzzle puzzle = null;
        if(sender instanceof Player) puzzle = Puzzle.get(((Player) sender).getWorld());
        if(sender instanceof BlockCommandSender) puzzle = Puzzle.get(((BlockCommandSender) sender).getBlock().getWorld());
        return command.execute(sender, puzzle, newArgs, prefix);
    }

    private void showHelp(CommandSender sender, List<CommandLeaf> accessibleCommands) {
        if(accessibleCommands.isEmpty()) return;
        sender.sendMessage(prefix + ChatColor.GRAY + "---------------------------------");
        sender.sendMessage(prefix + ChatColor.GOLD + "" + ChatColor.BOLD + baseCommand + " Commands");
        sender.sendMessage(prefix + ChatColor.GRAY + "---------------------------------");
        sender.sendMessage(prefix + ChatColor.GREEN + "(Click a command to autofill to your chat)");
        accessibleCommands.forEach(c -> {
            String suffix = "";
            if(c.getAliases().length > 1) {
                suffix = "(" + Arrays.toString(Arrays.copyOfRange(c.getAliases(), 1, c.getAliases().length)) + ")";
            }
            String commandExample = prefix + ChatColor.GOLD + "/" + baseCommand + " " + c.getAliases()[0] + suffix;
                sender.spigot().sendMessage(new ComponentBuilder(commandExample)
                        .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,  "/" + baseCommand + " " + c.getAliases()[0]))
                        .create());
            Arrays.stream(c.getHelpMessage()).forEach(h -> sender.sendMessage(prefix + h));
        });
    }
}
